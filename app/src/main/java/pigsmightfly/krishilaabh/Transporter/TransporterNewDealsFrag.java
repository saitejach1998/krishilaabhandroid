package pigsmightfly.krishilaabh.Transporter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steelkiwi.library.view.BadgeHolderLayout;

import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.R;

public class TransporterNewDealsFrag extends Fragment {

    public static TransporterNewDealsFrag newInstance() {
        TransporterNewDealsFrag fragment = new TransporterNewDealsFrag();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transporter_new_deals, container, false);
       final EditText code = view.findViewById(R.id.trans_unique_code);
       final CardView uniq = view.findViewById(R.id.CheckUniq);
        uniq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(code.getText().length()==0){
                    code.setError("Please enter a code");
                    return;
                }
                //NetworkRequest
                loadDeals(code.getText().toString());





            }
        });

        return view;

    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container_transporter, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    ///Network REquest

    public void loadDeals(String uniq) {
        final String uniqId = uniq;
        RequestQueue myqueue = Volley.newRequestQueue(getContext());
        final android.support.v7.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new android.support.v7.app.AlertDialog.Builder(getContext());
        }
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST,  "http://192.168.43.65:5012/verify-code", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                if (response.compareTo("not-found")==0) {

                builder.setTitle("Code error")
                        .setMessage(" The Unique id does not exist ")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }

                        })
                        .setIcon(android.R.drawable.alert_light_frame)
                        .show();
                }
                else {

                    TransporterViewTripFrag t  = new TransporterViewTripFrag();
                    Bundle b  = new Bundle();
                    b.putString("res",response);
                    b.putString("uniq",uniqId);
                    t.setArguments(b);
                    replaceFragment(t);

                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.


                builder.setTitle("Connection error")
                        .setMessage(" Failed to reach the server\n Please try again later ")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {                            }

                        })
                        .setIcon(android.R.drawable.alert_light_frame)
                        .show();
            }

        }) {
            protected Map<String, String> getParams() {

                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("uniq", uniqId); //Add the data you'd like to send to the server.
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);


    }



    
}
