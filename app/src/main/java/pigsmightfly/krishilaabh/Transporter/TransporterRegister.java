package pigsmightfly.krishilaabh.Transporter;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.Farmer.FarmerClass;
import pigsmightfly.krishilaabh.Farmer.FarmerLanding;
import pigsmightfly.krishilaabh.Farmer.RegisterPageFarmer;
import pigsmightfly.krishilaabh.R;

import static pigsmightfly.krishilaabh.Farmer.FarmerLogin.server;

public class TransporterRegister extends AppCompatActivity {
    TransporterClass newTrans;
    EditText trans_agency,transAddress,trans_pnumber,agency_city,agency_state;
    Spinner trans_type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transporter_register);
        TextView getStarted = findViewById(R.id.getstarted_transporter);
        trans_agency = findViewById(R.id.agency);
        transAddress = findViewById(R.id.transAddress);
        trans_pnumber = findViewById(R.id.trans_pnumber);
        trans_type = (Spinner)findViewById(R.id.dropdownTransporter);
        agency_city = findViewById(R.id.TranporterRegisterCity);
        agency_state  = findViewById(R.id.TransporterRegisterState);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.transportList, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        trans_type.setAdapter(adapter);

        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(trans_agency.getText().toString().length()==0) {
                    trans_agency.setError("Please enter an Agency Name");
                    return;
                }
                if(transAddress.getText().toString().length()==0) {
                    transAddress.setError("Please enter an Address");
                    return;
                }
                if(trans_pnumber.getText().toString().length()==0) {
                    trans_pnumber.setError("Please enter a phone number");
                    return;
                }
                if(agency_city.getText().toString().length()==0) {
                    agency_city.setError("Please enter a city");
                    return;
                }if(agency_state.getText().toString().length()==0) {
                    agency_state.setError("Please enter a state");
                    return;
                }
                newTrans = new TransporterClass(trans_agency.getText().toString(),transAddress.getText().toString(),trans_pnumber.getText().toString(),trans_type.getSelectedItem().toString(),agency_city.getText().toString(),agency_state.getText().toString());
                sendTransDetails(newTrans);



            }
        });


    }


    private void sendTransDetails(final TransporterClass trans){
        final TransporterClass t = trans;
        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(TransporterRegister.this, android.R.style.Theme_DeviceDefault_Light_Dialog_Alert);
           builder.setCancelable(false);

        } else {
            builder = new AlertDialog.Builder(TransporterRegister.this);
            builder.setCancelable(false);
        }
        RequestQueue myqueue = Volley.newRequestQueue(this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, "http://192.168.43.65:5012"+"/add-transport", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //dialog.dismiss();
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                if(response.compareTo("user_added") == 0 ) {
                    builder.setTitle("Registration Success")
                            .setMessage("You have Successfully registered!!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    System.out.println("User Added");
                                    SharedPreferences.Editor editor = getSharedPreferences("P", MODE_PRIVATE).edit();
                                    editor.putString("User", "Transporter");
                                    editor.putBoolean("isFirstRun", false);
                                    editor.apply();
                                    /*
                                    editor.putString("Name", f.name);
                                    editor.putString("Address", f.Address);
                                    editor.putString("uid", f.uid);
                                    editor.putString("City",f.city);
                                    editor.putString("State",f.state);

                                    */
                                    Intent i = new Intent(TransporterRegister.this, TransporterLanding.class);
                                    startActivity(i);

                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }
                else if(response.compareTo("duplicate")==0){
                    builder.setTitle("Registration Failed")
                            .setMessage("A Duplicate User was detected")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }
                else{
                    System.out.println(response);

                    builder.setTitle("Registration Failed")
                            .setMessage("Network Error!!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
               // dialog.dismiss();
                builder.setTitle("Connection  Failure")
                        .setMessage(" Could not reach the server\n Please try again later")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }

                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() {

                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("Agency", t.agency);
                MyData.put("Address", t.address);
                MyData.put("Phone", t.phone_number);
                MyData.put("Type",t.type);
                MyData.put("City",t.agency_city);
                MyData.put("State",t.agency_state);
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);



    }

}
