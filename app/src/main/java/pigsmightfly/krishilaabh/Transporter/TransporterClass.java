package pigsmightfly.krishilaabh.Transporter;

/**
 * Created by saite on 3/25/2018.
 */

public class TransporterClass {
    String agency;
    String address;
    String phone_number;
    String type;
    String agency_city;
    String agency_state;

    public void setAgency_city(String agency_city) {
        this.agency_city = agency_city;
    }

    public void setAgency_state(String agency_state) {
        this.agency_state = agency_state;
    }

    public String getAgency_city() {

        return agency_city;
    }

    public String getAgency_state() {
        return agency_state;
    }

    public TransporterClass(String agency, String address, String phone_number, String type, String agency_city, String agency_state) {
        this.agency = agency;
        this.address = address;
        this.phone_number = phone_number;
        this.type = type;
        this.agency_city = agency_city;
        this.agency_state = agency_state;
    }

    public TransporterClass(String agency, String address, String phone_number, String type) {
        this.agency = agency;
        this.address = address;
        this.phone_number = phone_number;
        this.type = type;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAgency() {

        return agency;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getType() {
        return type;
    }
}
