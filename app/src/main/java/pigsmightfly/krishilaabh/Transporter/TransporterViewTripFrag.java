package pigsmightfly.krishilaabh.Transporter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steelkiwi.library.view.BadgeHolderLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.R;

public class TransporterViewTripFrag extends Fragment {

    String showReceivedData,UniqId;
    TextView pickupname,pickupphone,pickupaddr,pickupcity,pickupstate,dropname,dropphone,dropaddr,dropcity,dropstate;
    CardView con_Deal;
    public static TransporterViewTripFrag newInstance(){

        TransporterViewTripFrag fragment  = new TransporterViewTripFrag();

        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            showReceivedData = args.getString("res");
            UniqId = args.getString("uniq");

        }

        System.out.println("onStart" + showReceivedData);

    }
    @Override
    public void onStart() {
        super.onStart();

    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_route, container, false);
       CardView confirmTrip = view.findViewById(R.id.Trans_pick_drop_confirmbutton);

       //grab..
       pickupname = view.findViewById(R.id.Trans_pick_drop_name_pickup);
       dropname = view.findViewById(R.id.Trans_pick_drop_name_drop);
       pickupaddr = view.findViewById(R.id.Trans_pick_drop_address_pickup);
       dropaddr = view.findViewById(R.id.Trans_pick_drop_address_drop);
       pickupphone = view.findViewById(R.id.Trans_pick_drop_Phone_pickup);
       dropphone = view.findViewById(R.id.Trans_pick_drop_Phone_drop);
       pickupcity = view.findViewById(R.id.Trans_pick_drop_city_pickup);
       dropcity = view.findViewById(R.id.Trans_pick_drop_city_drop);
       pickupstate = view.findViewById(R.id.Trans_pick_drop_state_pickup);
       dropstate = view.findViewById(R.id.Trans_pick_drop_state_drop);
        con_Deal  = view.findViewById(R.id.Trans_pick_drop_confirmbutton);


        con_Deal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        loadDetails(showReceivedData);
       //pickup

       confirmTrip.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               //Network request
                //On Sucess


               ///Open My deals fragment
               loadDeals(showReceivedData);

               ((TransporterLanding)getActivity()).loadFragment(new TransporterNewDealsFrag());
           }
       });

        return view;

    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container_transporter, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public void loadDetails(String res){

        System.out.println("LoadDeals" + res);
        try{
            JSONArray t  = new JSONArray(res);
            JSONObject o = t.getJSONObject(0);
            dropname.setText(o.getString("Name"));
            dropphone.setText(o.getString("Phone"));
            dropaddr.setText(o.getString("Address"));
            dropcity.setText(o.getString("City"));
            dropstate.setText(o.getString("State"));


            o = t.getJSONObject(1);

            pickupname.setText(o.getString("Name"));
            pickupphone.setText(o.getString("Phone"));
            pickupaddr.setText(o.getString("Address"));
            pickupcity.setText(o.getString("City"));
            pickupstate.setText(o.getString("State"));


        }
        catch (JSONException r){
            r.printStackTrace();
        }




    }
    ///Network REquest
    public void loadDeals(String res) {
        final String response1 = res;
        RequestQueue myqueue = Volley.newRequestQueue(getContext());
        final android.support.v7.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new android.support.v7.app.AlertDialog.Builder(getContext());
        }
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST,  "http://192.168.43.65:5012/upcoming-deal", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                if (response.compareTo("done")==0) {

                    builder.setTitle("Deal Confirmed")
                            .setMessage("Deal has been confirmed!!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.


                builder.setTitle("Connection error")
                        .setMessage(" Failed to reach the server\n Please try again later ")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {                            }

                        })
                        .setIcon(android.R.drawable.alert_light_frame)
                        .show();
            }

        }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                try{

                    JSONArray t = new JSONArray(response1);
                    JSONObject o = t.getJSONObject(0);
                    MyData.put("WholesalerDetails",o.toString());
                    o = t.getJSONObject(1);
                    MyData.put("FarmerDetails",o.toString());
                    MyData.put("Email","saiteja.ch1998@gmail.com");
                    MyData.put("uniq",UniqId);


                }
                catch (JSONException e){
                    e.printStackTrace();
                }
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);


    }






}
