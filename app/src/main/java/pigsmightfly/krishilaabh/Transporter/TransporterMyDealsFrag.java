package pigsmightfly.krishilaabh.Transporter;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.steelkiwi.library.view.BadgeHolderLayout;

import pigsmightfly.krishilaabh.R;

public class TransporterMyDealsFrag extends Fragment {
    public int cartcount;
    RecyclerView recycler;
    private ProgressDialog dialog;
    BadgeHolderLayout badgeHolderLayout;
    ImageButton cartButton;
    public static TransporterMyDealsFrag newInstance() {
        TransporterMyDealsFrag fragment = new TransporterMyDealsFrag();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_deals, container, false);
        return view;

    }




}
