package pigsmightfly.krishilaabh.Farmer;

/**
 * Created by saite on 3/13/2018.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pigsmightfly.krishilaabh.R;

/**
 * Created by saite on 3/13/2018.
 */

public class DealsAdapter extends RecyclerView.Adapter<DealsAdapter.ProductViewHolder> {


    private Context mCtx;
    private ArrayList<DealClass> dealList;

    public DealsAdapter(Context mCtx, ArrayList<DealClass> dealList) {
        this.mCtx = mCtx;
        this.dealList = dealList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.deal_list2, null);
        return new ProductViewHolder(view);
    }




    public void onBindViewHolder(ProductViewHolder holder, int position) {
        DealClass deal = dealList.get(position);
        holder.cropName.setText(deal.getProduct());
        holder.quantity.setText(deal.getQuantity()+" "+deal.getUnits());
        holder.variety.setText(deal.getVariety());
    }

    @Override
    public int getItemCount() {
        return dealList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView cropName, quantity, price,variety;


        public ProductViewHolder(View itemView) {
            super(itemView);
            cropName = itemView.findViewById(R.id.CropName);
            quantity = itemView.findViewById(R.id.quantity);
            price = itemView.findViewById(R.id.price);
            variety = itemView.findViewById(R.id.variety);

        }
    }
}
