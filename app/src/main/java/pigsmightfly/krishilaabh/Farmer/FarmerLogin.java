package pigsmightfly.krishilaabh.Farmer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.R;


//implementing onclicklistener
//17.3.18 -> add here(FarmerLogin -> Farmer Login + LoginRegister)
public class FarmerLogin extends AppCompatActivity {

    //View Objects
    private CardView buttonScan,buttonLogin,buttonSignUp,createAccount;
    private TextView textViewName, textViewAddress;
    private EditText uname,password;

    public final static String server = "http://192.168.43.65:5000";
   private ProgressDialog dialog;
    //qr code scanner object
    //private IntentIntegrator qrScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_login);


        TextView textView = (TextView) findViewById(R.id.login);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");
        textView.setTypeface(typeface);

        TextView textView1 = (TextView) findViewById(R.id.accText);
        createAccount = (CardView)findViewById(R.id.createAccount);
        textView1.setTypeface(typeface);

        TextView textView2 = (TextView) findViewById(R.id.SignUp);
        Typeface typeface1 = Typeface.createFromAsset(getAssets(),"fonts/text.ttf");
        textView2.setTypeface(typeface1);

        //View objects
       // buttonScan = (CardView) findViewById(R.id.buttonScan);
        buttonLogin = (CardView) findViewById(R.id.Login);
       // buttonSignUp = (CardView) findViewById(R.id.SignUp);
        uname = (EditText)findViewById(R.id.uname);
        password = (EditText)findViewById(R.id.pwd);
        //TextView Signup  = (TextView)findViewById(R.id.SignUp);

        /*
        SpannableString content = new SpannableString("New to The App? Click here to get started");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        Signup.setText(content);

        */
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FarmerLogin.this,FarmerRegister.class);
                startActivity(i);
            }
        });

        //intializing scan object
        //qrScan = new IntentIntegrator(this);

        //attaching onclick listener
        //buttonScan.setOnClickListener(this);


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(uname.getText().length()==0) {
                    uname.setError("Please Enter a UID");
                    return;
                }
                if(uname.getText().length() != 12){
                    uname.setError("Please Enter a Valid Aadhaar UID");
                    return;
                }

                if(password.getText().length()==0) {
                    password.setError("Please Enter Password");
                    return;
                }

                LoginVerify(uname.getText().toString(),getSHA256Hash(password.getText().toString()));
                      dialog = ProgressDialog.show(FarmerLogin.this, "",
                                "Loading. Please wait...", true);
            }
        });


/*
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FarmerLogin.this,RegisterPageFarmer.class);
                startActivity(i);
            }
        });
        */
    }
/*
    //Getting the scan results
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                Intent i = new Intent(FarmerLogin.this,RegisterPageFarmer.class);
                i.putExtra("qr_data",result.getContents());
                startActivity(i);


            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void onClick(View view) {
        //initiating the qr code scan
        qrScan.initiateScan();
    }

*/

    private void LoginVerify(String uid,String pwdHash){

        final String UID,passhash;
        UID = uid;
        passhash = pwdHash;

        RequestQueue myqueue = Volley.newRequestQueue(this);
       final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           builder  = new AlertDialog.Builder(FarmerLogin.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(FarmerLogin.this);
        }
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, server+"/sign-in", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    dialog.dismiss();
                    final JSONObject res = new JSONObject(response);
                    final Boolean loginres = res.getBoolean("Authenticated");
                    //This code is executed if the server responds, whether or not the response contains data.
                    //The String 'response' contains the server's response.
                    if (loginres) {
                        builder.setTitle("Login Success")
                                                .setMessage("You have Successfully Logged in!!")
                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        System.out.println("Logged in");
                                                        SharedPreferences.Editor editor = getSharedPreferences("P", MODE_PRIVATE).edit();
                                                        editor.putString("User", "Farmer");
                                                        editor.putBoolean("isFirstRun", false);
                                                        try {
                                                            editor.putString("Name", res.getString("Name"));
                                                            editor.putString("Address", res.getString("Address"));
                                                            editor.putString("City", res.getString("City"));
                                                            editor.putString("State", res.getString("State"));
                                                            editor.putString("uid", res.getString("UID"));
                                                        }
                                                        catch (JSONException r){
                                                            r.printStackTrace();
                                                        }
                                        editor.apply();
                                        Intent i = new Intent(FarmerLogin.this, FarmerLanding.class);
                                        startActivity(i);
                                    }

                                })
                                .setIcon(android.R.drawable.alert_light_frame)
                                .show();
                    } else if (loginres==false) {
                        builder.setTitle("Login Failure")
                                .setMessage("You have Entered a wrong username password")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }

                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    } else {
                        builder.setTitle("Login Failure")
                                .setMessage("Please check your internet connection")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }

                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                }
                catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                    dialog.dismiss();
                    builder.setTitle("Connection  Failure")
                            .setMessage(" Could not reach the server\n Please try again later")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }

                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

        }) {
            protected Map<String, String> getParams() {

                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("UID",UID ); //Add the data you'd like to send to the server.
                MyData.put("pwdHash", passhash);
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);



    }


    private String getSHA256Hash(String data) {
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes("UTF-8"));
            return (bytesToHex(hash)); // make it printable
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private String bytesToHex(byte[] bytes){

        char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}