package pigsmightfly.krishilaabh.Farmer;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.R;


//implementing onclicklistener
public class FarmerRegister extends AppCompatActivity implements View.OnClickListener {

    //View Objects
    private CardView buttonScan, buttonSignUp;
    private TextView textViewName, textViewAddress;
    private EditText uname, password;
    public final static String server = "http://192.168.43.65:5000";
    //qr code scanner object
    private IntentIntegrator qrScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_register);
        TextView textView = (TextView) findViewById(R.id.registertext);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/text.ttf");
        textView.setTypeface(typeface);

        //View objects
        buttonScan = (CardView) findViewById(R.id.buttonScan);

        buttonSignUp = (CardView) findViewById(R.id.SignUp);
        uname = (EditText) findViewById(R.id.uname);
        password = (EditText) findViewById(R.id.pwd);


        //intializing scan object
        qrScan = new IntentIntegrator(this);

        //attaching onclick listener
        buttonScan.setOnClickListener(this);


        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FarmerRegister.this, RegisterPageFarmer.class);
                startActivity(i);
            }
        });
    }

    //Getting the scan results
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                Intent i = new Intent(FarmerRegister.this, RegisterPageFarmer.class);
                i.putExtra("qr_data", result.getContents());
                startActivity(i);


            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View view) {
        //initiating the qr code scan
        qrScan.initiateScan();
    }
}