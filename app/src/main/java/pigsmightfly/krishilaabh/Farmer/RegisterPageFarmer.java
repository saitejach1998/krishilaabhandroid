package pigsmightfly.krishilaabh.Farmer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import pigsmightfly.krishilaabh.R;

import static pigsmightfly.krishilaabh.Farmer.FarmerDeals.url;
import static pigsmightfly.krishilaabh.Farmer.FarmerLogin.server;

import java.security.MessageDigest;

public class RegisterPageFarmer extends AppCompatActivity {
 private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register_page_farmer);


        Typeface typeface1 = Typeface.createFromAsset(getAssets(),"fonts/text.ttf");


        String qr_value = null;
        Map<String, String> user_data;

        final EditText name,address,id,pwd,city,state;
        name = (EditText)findViewById(R.id.nameText);
        address = (EditText)findViewById(R.id.AddressText);
        id = (EditText)findViewById(R.id.AadharText);
        pwd = (EditText)findViewById(R.id.passwordText);
        city = (EditText)findViewById(R.id.CityEditText);
        state = (EditText)findViewById(R.id.StateEditText);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            qr_value = extras.getString("qr_data");
            final Map<String, String> parsed_Data =  parse(qr_value);
            user_data = parsed_Data;
            name.setText(user_data.get("name"));
            String addr = parsed_Data.get("house")+" "+parsed_Data.get("street")+" "+parsed_Data.get("lm")+" "+parsed_Data.get("loc")+" "+parsed_Data.get("vtc")+" "+parsed_Data.get("po");
            address.setText(addr);
            city.setText(parsed_Data.get("dist"));
            state.setText(parsed_Data.get("state"));
            id.setText(user_data.get("uid"));


            //The key argument here must match that used in the other activity
        }



        CardView confirm = (CardView) findViewById(R.id.confirmSubmit);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(name.getText().length()==0) {
                    name.setError("Please Enter Name");
                    return;
                }
                if(id.getText().length()==0) {
                    id.setError("Please Enter UID");
                    return;
                }

                if(id.getText().length() != 12){
                    id.setError("Please Enter a Valid Aadhaar UID");
                    return;
                }
                if(address.getText().length()==0) {
                    address.setError("Please Enter Address");
                    return;
                }
                if(pwd.getText().length()==0) {
                    pwd.setError("Please Enter a password");
                    return;
                }
                if(city.getText().length()==0) {
                    city.setError("Please Enter a City");
                    return;
                }
                if(state.getText().length()==0) {
                    state.setError("Please Enter a State");
                    return;
                }
                FarmerClass f = new FarmerClass();
                f.name = name.getText().toString();
                f.uid = id.getText().toString();
                f.Address = address.getText().toString();
                f.pwdHash = getSHA256Hash(pwd.getText().toString());
                f.city = city.getText().toString();
                f.state = state.getText().toString();
                sendFarmerDetails(f);
                dialog = ProgressDialog.show(RegisterPageFarmer.this, "",
                                "Loading. Please wait...", true);


            }
        });

    }
//192.168.43.209:5000
    private void sendFarmerDetails(FarmerClass farmer){
        final FarmerClass f = farmer;
       final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(RegisterPageFarmer.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(RegisterPageFarmer.this);
        }
        RequestQueue myqueue = Volley.newRequestQueue(this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, server+"/submit-data", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                if(response.compareTo("user_added") == 0 ) {
                    builder.setTitle("Registration Success")
                            .setMessage("You have Successfully registered!!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    System.out.println("User Added");
                                    SharedPreferences.Editor editor = getSharedPreferences("P", MODE_PRIVATE).edit();
                                    editor.putString("User", "Farmer");
                                    editor.putBoolean("isFirstRun", false);
                                    editor.putString("Name", f.name);
                                    editor.putString("Address", f.Address);
                                    editor.putString("uid", f.uid);
                                    editor.putString("City",f.city);
                                    editor.putString("State",f.state);
                                    editor.apply();
                                    Intent i = new Intent(RegisterPageFarmer.this, FarmerLanding.class);
                                    startActivity(i);
                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }
                else if(response.compareTo("duplicate")==0){
                    builder.setTitle("Registration Failed")
                            .setMessage("A Duplicate User was detected")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }
                else{
                    System.out.println(response);

                    builder.setTitle("Registration Failed")
                            .setMessage("Network Error!!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                dialog.dismiss();
                builder.setTitle("Connection  Failure")
                        .setMessage(" Could not reach the server\n Please try again later")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }

                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() {

                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("UID", f.uid); //Add the data you'd like to send to the server.
                MyData.put("Name", f.name);
                MyData.put("Address", f.Address);
                MyData.put("pwdHash", f.pwdHash);
                MyData.put("City",f.city);
                MyData.put("State",f.state);
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);



    }

    Map<String, String> parse(String args) {
        // TODO Auto-generated method stub


        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<PrintLetterBarcodeData uid=\"936763306827\" name=\"Chodavarapu Saiteja\" gender=\"M\" yob=\"1998\" co=\"S/O: C.H V Satyanarayana Murty\" house=\"92\" street=\"kalyan gardens\" lm=\"near ramakrishna talent school\" loc=\"sainik puri\" vtc=\"Tirumalagiri\" po=\"Sainikpuri\" dist=\"Hyderabad\" subdist=\"Tirumalagiri\" state=\"Andhra Pradesh\" pc=\"500094\"/>";

        DocumentBuilder db;
        try {
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(args));

            try {
                Document doc = db.parse(is);

                NodeList nodes = doc.getElementsByTagName("PrintLetterBarcodeData");

                Element element = (Element) nodes.item(0);

                Map<String, String> map = new HashMap<String, String>();

                map.put("uid", element.getAttribute("uid"));
                map.put("name", element.getAttribute("name"));
                map.put("gender", element.getAttribute("gender"));
                map.put("yob", element.getAttribute("yob"));
                map.put("co", element.getAttribute("co"));
                map.put("house", element.getAttribute("house"));
                map.put("street", element.getAttribute("street"));
                map.put("lm", element.getAttribute("lm"));
                map.put("loc", element.getAttribute("loc"));
                map.put("vtc", element.getAttribute("vtc"));
                map.put("po", element.getAttribute("po"));
                map.put("dist", element.getAttribute("dist"));
                map.put("subdist", element.getAttribute("subdist"));
                map.put("state", element.getAttribute("state"));
                map.put("pc", element.getAttribute("pc"));

                for (Map.Entry m : map.entrySet()) {
                    System.out.println(m.getKey() + " " + m.getValue());
                }
                return map;

            } catch (SAXException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }


        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }


    }

    private String getSHA256Hash(String data) {
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes("UTF-8"));
            return (bytesToHex(hash)); // make it printable
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private String bytesToHex(byte[] bytes){

        char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
