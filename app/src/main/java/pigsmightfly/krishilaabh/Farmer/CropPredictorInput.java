package pigsmightfly.krishilaabh.Farmer;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pigsmightfly.krishilaabh.R;

public class CropPredictorInput extends AppCompatActivity {
    Spinner predictor_state,predictor_district,predictor_soil,predictor_season;
    EditText predictor_production,predictor_area;
    CardView predict_button;
    ArrayAdapter<String> upper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_predictor_input);
        List<String> states = new ArrayList<String>();
        final List<List<String>> districts = new ArrayList<List<String>>();
        predictor_state = (Spinner)findViewById(R.id.CropPredictorState);
        predictor_district = (Spinner)findViewById(R.id.CropPredictorDistrict);
        predictor_soil = (Spinner)findViewById(R.id.CropPredictorSoilType);
        predictor_production = findViewById(R.id.CropPredictorProduction);
        predictor_season = (Spinner) findViewById(R.id.CropPredictorSeason) ;
        predictor_area = findViewById(R.id.CropPredictorArea);
        predict_button  = (CardView) findViewById(R.id.predict_button);




        try{
            JSONObject data = new JSONObject(loadJSONFromAsset());
            Iterator<?> keys = data.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                states.add(key);
                districts.add(Arrays.asList(data.get(key).toString().split("_")));
            }



        }
        catch (JSONException r){
            r.printStackTrace();
        }




        final ArrayAdapter<String> ia = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, states);
        ia.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        predictor_state.setAdapter(ia);

        predictor_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                upper = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, districts.get(position));
                upper.notifyDataSetChanged();
                upper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                predictor_district.setAdapter(upper);

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        predict_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(predictor_production.getText().length()==0){
                    predictor_production.setError("Please Enter Production");
                    return;
                }

                if(predictor_area.getText().length()==0){
                    predictor_area.setError("Please Enter Area");
                    return;
                }
                sendInput(Double.parseDouble(predictor_production.getText().toString()),Double.parseDouble(predictor_area.getText().toString()));
            }
        });


    }


    public void sendInput(Double P,Double A) {
        final Double Pro = P;
        final Double Area = A;
        RequestQueue myqueue = Volley.newRequestQueue(this);
        final android.support.v7.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new android.support.v7.app.AlertDialog.Builder(this);
        }
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST,  "http://192.168.43.65:5000/predict", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                if (response.compareTo("not-found")==0) {
                    System.out.println("Response is null");

                }
                else {
                    Intent i  = new Intent(CropPredictorInput.this,PredictorResultPage.class);
                    i.putExtra("response",response);
                    i.putExtra("old_efficiency",Pro/Area);
                    i.putExtra("production",Pro);
                    startActivity(i);

                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.


                builder.setTitle("Connection error")
                        .setMessage(" Failed to reach the server\n Please try again later ")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                            }

                        })
                        .setIcon(android.R.drawable.alert_light_frame)
                        .show();
            }

        }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("District",predictor_district.getSelectedItem().toString());
                MyData.put("Soil",predictor_soil.getSelectedItem().toString());
                MyData.put("Season",predictor_season.getSelectedItem().toString());

                System.out.println(MyData);
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);


    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("statesdata.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
