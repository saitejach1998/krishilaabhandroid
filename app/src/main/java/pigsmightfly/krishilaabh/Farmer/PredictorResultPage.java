package pigsmightfly.krishilaabh.Farmer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

import pigsmightfly.krishilaabh.R;

public class PredictorResultPage extends AppCompatActivity {

    TextView crop1,crop2,crop3,labh1,labh2,labh3,pro1,pro2,pro3;
    Double new_eff1,new_eff2,new_eff3,old_eff,old_prod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predictor_result_page2);
        Intent i = getIntent();
        crop1 = findViewById(R.id.PredPageCropName1);
        crop2 = findViewById(R.id.PredPageCropName2);
        crop3 = findViewById(R.id.PredPageCropName3);

        labh1 = findViewById(R.id.PredPageLaabh1);
        labh2 = findViewById(R.id.PredPageLaabh2);
        labh3 = findViewById(R.id.PredPageLaabh3);


        pro1 = findViewById(R.id.PredPageProduction1);
        pro2 = findViewById(R.id.PredPageProduction2);
        pro3 = findViewById(R.id.PredPageProduction3);
        String response = i.getStringExtra("response");
        old_eff = i.getDoubleExtra("old_efficiency",0);
        old_prod = i.getDoubleExtra("production",0);

        try{

            JSONObject recv = new JSONObject(response);
            crop1.setText(recv.getString("first_crop"));
            crop2.setText(recv.getString("second_crop"));
            crop3.setText(recv.getString("third_crop"));
            new_eff1 = Double.parseDouble(recv.getString("first_yield"));
            new_eff2 = Double.parseDouble(recv.getString("second_yield"));
            new_eff3 = Double.parseDouble(recv.getString("third_yield"));
            Double percentage1 =  ((new_eff1 - old_eff)/old_eff)*100;
            Double percentage2 =  ((new_eff2 - old_eff)/old_eff)*100;
            Double percentage3 =  ((new_eff3 - old_eff)/old_eff)*100;
            DecimalFormat df = new DecimalFormat("#.##");

            labh1.setText("Laabh"+"     "+df.format(percentage1).toString()+"%");
            labh2.setText("Laabh"+"     "+df.format(percentage2).toString()+"%");
            labh3.setText("Laabh"+"     "+df.format(percentage3).toString()+"%");


            Double new_prod1 = old_prod+((percentage1/100)*old_prod);
            Double new_prod2 = old_prod+((percentage2/100)*old_prod);
            Double new_prod3 = old_prod+((percentage3/100)*old_prod);

            pro1.setText("Achievable Production: "+df.format(new_prod1).toString()+" "+"tonnes");
            pro2.setText("Achievable Production: "+df.format(new_prod2).toString()+" "+"tonnes");
            pro3.setText("Achievable Production: "+df.format(new_prod3).toString()+" "+"tonnes");
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }
}
