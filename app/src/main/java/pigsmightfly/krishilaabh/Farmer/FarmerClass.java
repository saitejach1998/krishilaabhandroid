package pigsmightfly.krishilaabh.Farmer;

/**
 * Created by saite on 3/13/2018.
 */

public class FarmerClass {
    public FarmerClass() {

    }

    public FarmerClass(String name, String uid, String address) {
        this.name = name;
        this.uid = uid;
        Address = address;
    }

    String name;
    String uid;
    String Address;
    String pwdHash;
    String city;
    String state;

    public FarmerClass(String name, String uid, String address, String pwdHash, String city, String state) {
        this.name = name;
        this.uid = uid;
        Address = address;
        this.pwdHash = pwdHash;
        this.city = city;
        this.state = state;
    }
}
