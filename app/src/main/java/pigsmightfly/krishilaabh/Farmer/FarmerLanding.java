package pigsmightfly.krishilaabh.Farmer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.graphics.Typeface;
import android.widget.TextView;

import pigsmightfly.krishilaabh.R;

public class FarmerLanding extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_landing);
        SharedPreferences prefs = getSharedPreferences("P",MODE_PRIVATE);
        TextView farmername = (TextView)findViewById(R.id.farmer_name_text);
        farmername.setText(prefs.getString("Name","Farmer"));
        TextView textView = (TextView) findViewById(R.id.deals_text);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");
        textView.setTypeface(typeface);

        TextView textView1 = (TextView) findViewById(R.id.infodesk_text);
        Typeface typeface1 = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");
        textView1.setTypeface(typeface1);

        TextView textView2 = (TextView) findViewById(R.id.crop_predict_text);
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");
        textView2.setTypeface(typeface2);

        CardView urDeals = (CardView)findViewById(R.id.deals);
        CardView infodesk = (CardView) findViewById(R.id.infodesk);
        CardView predict = (CardView) findViewById(R.id.crop_predict);

        urDeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FarmerLanding.this,FarmerDeals.class);
                startActivity(i);

            }
        });
        predict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FarmerLanding.this,CropPredictorInput.class);
                startActivity(i);
            }
        });
        /*Button n=(Button) findViewById(R.id.deals);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");

        n.setTypeface(typeface);

        Button n1=(Button) findViewById(R.id.infodesk);
        Typeface typeface1 = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");

        n1.setTypeface(typeface1);

        Button n2=(Button) findViewById(R.id.crop_predict);
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");

        n2.setTypeface(typeface2);
        */
        /*Button urDeals = (Button)findViewById(R.id.deals);
        Button infodesk = (Button)findViewById(R.id.infodesk);
        Button predict = (Button)findViewById(R.id.crop_predict);

        urDeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FarmerLanding.this,FarmerDeals.class);
                startActivity(i);

            }
        });*/
    }


    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory( Intent.CATEGORY_HOME );
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
        return;
    }
}
