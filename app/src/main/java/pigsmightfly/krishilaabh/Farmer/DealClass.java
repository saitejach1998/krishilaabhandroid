package pigsmightfly.krishilaabh.Farmer;

import java.io.Serializable;

/**
 * Created by saiteja on 3/11/2018.
 */

public class DealClass implements Serializable {

    String product;
    double quantity;
    String Variety;
    String units;
    String id;
    String sellername;
    String address;
    String price;

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSellername() {
        return sellername;
    }

    public String getAddress() {
        return address;
    }

    public DealClass(String product, double quantity, String variety, String units, String id, String sellername, String address, String price) {
        this.product = product;
        this.quantity = quantity;
        Variety = variety;
        this.units = units;
        this.id = id;
        this.sellername = sellername;
        this.address = address;
        this.price = price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {

        return price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {

        return id;
    }

    public DealClass(String product, double quantity, String variety, String units, String id) {
        this.product = product;
        this.quantity = quantity;
        Variety = variety;
        this.units = units;
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getVariety() {
        return Variety;
    }

    public void setVariety(String variety) {
        Variety = variety;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public DealClass() {
    }

    public DealClass(String product, double quantity, String variety, String units) {
        this.product = product;
        this.quantity = quantity;
        Variety = variety;
        this.units = units;
    }
}
