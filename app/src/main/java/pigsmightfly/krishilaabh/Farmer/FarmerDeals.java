package pigsmightfly.krishilaabh.Farmer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pigsmightfly.krishilaabh.R;

import static android.content.ContentValues.TAG;
import static pigsmightfly.krishilaabh.Farmer.FarmerLogin.server;

public class FarmerDeals extends AppCompatActivity {
    public static String url ="http://192.168.43.65:5000/get-deal";
    private ProgressDialog dialog;
    Dialog myDialog;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_deals);



        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    //loading deal page
        loadDeals();
        dialog = ProgressDialog.show(FarmerDeals.this, "",
                "Loading. Please wait...", true);
        //diaglog initiazilaton
        myDialog = new Dialog(this);
        CardView b = findViewById(R.id.new_deal);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopup(view);
            }
        });
    }
    public void ShowPopup(View v) {
        TextView txtclose;
        CardView btndeals;
        myDialog.setContentView(R.layout.custompopup);
        txtclose =(TextView) myDialog.findViewById(R.id.txtclose);
        txtclose.setText("X");
        btndeals =  myDialog.findViewById(R.id.deal_post);
        final EditText products = (EditText) myDialog.findViewById(R.id.product);
        final EditText quantity = (EditText) myDialog.findViewById(R.id.quantity);
       // final Spinner unitSelect = (Spinner)findViewById(R.id.UnitsSpinner);
        /*
        ArrayAdapter<CharSequence> adapter =    ArrayAdapter.createFromResource(this,R.array.Units,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unitSelect.setAdapter(adapter);

        */
        final EditText units = (EditText) myDialog.findViewById(R.id.units);
        final EditText variety = (EditText) myDialog.findViewById(R.id.Variety);
        //Add Deal Button
            btndeals.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DealClass deal_new = new DealClass();

                    if(products.getText().toString().length()==0) {
                        products.setError("Please Enter Product Name");
                        return;
                    }
                    if(quantity.getText().toString().length()==0){
                        quantity.setError("Please Enter Quantity");
                        return;
                    }

                    if(units.getText().toString().length()==0){
                        units.setError("Please Enter Units");
                        return;
                    }


                    if(variety.getText().toString().length()==0) {
                        variety.setError("Please Enter Variety Name");
                        return;
                    }
                    deal_new.product = products.getText().toString();
                    deal_new.quantity = Float.valueOf(quantity.getText().toString());
                    deal_new.units = units.getText().toString();
                    deal_new.Variety = variety.getText().toString();
                    SharedPreferences prefs = getSharedPreferences("P",MODE_PRIVATE);

                    SendDeals(deal_new,prefs.getString("City","Dummy"));
                    dialog = ProgressDialog.show(FarmerDeals.this, "",
                            "Loading. Please wait...", true);
                    myDialog.dismiss();
                }
            });



        //Close Popup dialog
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
                loadDeals();
            }
        });



        //displaying dialog
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();


    }
    private void loadDeals(){
        final android.support.v7.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(FarmerDeals.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new android.support.v7.app.AlertDialog.Builder(FarmerDeals.this);
        }
        RequestQueue myqueue = Volley.newRequestQueue(this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, server+"/get-deal", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                dialog.dismiss();
                TextView Top = (TextView)findViewById(R.id.top);
                System.out.println(response);
                if(response.compareTo("no_deals")==0){
                    Top.setText("There are currently no deals posted");
                }

                else {
                    Top.setText("Your Deals");

                    loadDeals(response);

                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                System.out.println(error);
                dialog.dismiss();
                builder.setTitle("Connection  Failure")
                        .setMessage(" Could not reach the server\n Please try again later")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }

                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() {
                SharedPreferences prefs = getSharedPreferences("P", MODE_PRIVATE);
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("UID", prefs.getString("uid","uid does not exist")); //Add the data you'd like to send to the server.
                //MyData.put("Lat",)
                System.out.println(MyData);
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);



    }
    private void loadDeals(String response){
            ArrayList<DealClass> dealList = new ArrayList<DealClass>() {
            };
            //dialog.dismiss();
         try {
            //converting the string to json array object
            JSONArray array = new JSONArray(response);

            //traversing through all the object
            for (int i = 0; i < array.length(); i++) {

                //getting product object from json array
                JSONObject product = array.getJSONObject(i);

                //adding the product to product list
                dealList.add(new DealClass(
                        product.getString("Product"),product.getDouble("Quantity"),product.getString("Variety"),product.getString("Units")

                ));
            }

            //creating adapter object and setting it to recyclerview
            DealsAdapter adapter = new DealsAdapter(FarmerDeals.this, dealList);
           // RecyclerView r = findViewById(R.id.recyclerView);
            recyclerView.setAdapter(adapter);

            dialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void SendDeals(DealClass deal, final String city){
        final DealClass deal_new = deal;
        final RequestQueue myqueue = Volley.newRequestQueue(this);
        final android.support.v7.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(FarmerDeals.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new android.support.v7.app.AlertDialog.Builder(FarmerDeals.this);
        }

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, server+"/post-deal", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                if(response.compareTo("deal_added")==0){
                    builder.setTitle("Deal Posted")
                            .setMessage("You have Successfully posted a deal")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    loadDeals();
                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }
                if(response.compareTo("duplicate")==0){
                    builder.setTitle("Deal Not Posted")
                            .setMessage("There is a duplicate of the same deal in you current deals")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    loadDeals();
                                    dialog.dismiss();

                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }


            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                System.out.println(error);
                dialog.dismiss();
                builder.setTitle("Connection  Failure")
                        .setMessage(" Could not reach the server\n Please try again later")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }

                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() {
                SharedPreferences pref = getSharedPreferences("P",MODE_PRIVATE);
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("UID",pref.getString("uid","GlobalUID") ); //Add the data you'd like to send to the server.
                MyData.put("Product",deal_new.product);
                MyData.put("Quantity",Double.toString(deal_new.quantity));
                MyData.put("Units",deal_new.units);
                MyData.put("Variety",deal_new.Variety);
                MyData.put("City",city);
                System.out.println(MyData);
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);



    }





}







/*

class SendPostDetails extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {

        String data = "";

        HttpURLConnection httpURLConnection = null;
        try {

            httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            httpURLConnection.setRequestMethod("POST");

            httpURLConnection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes("FarmerData=" + params[1]);
            wr.flush();
            wr.close();

            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            int inputStreamData = inputStreamReader.read();
            while (inputStreamData != -1) {
                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                data += current;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }

        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result.compareTo("deal_added")==0)
            Log.d(TAG, "onPostExecute:"+result);

        else
            Log.d(TAG, "onPostExecute:"+result);

    }
}



 /*
        btndeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                JSONObject postData_Deals = new JSONObject();

                try{

                    postData_Deals.put("Product",deal_new.product);
                    postData_Deals.put("Quantity",deal_new.quantity);
                    postData_Deals.put("Units",deal_new.units);
                    postData_Deals.put("Variety",deal_new.Variety);
                    SharedPreferences pref = getSharedPreferences("P",MODE_PRIVATE);
                    postData_Deals.put("UID",pref.getString("uid","uid does not exist on local storage"));

                    SendPostDetails one = new SendPostDetails();
                    one.execute(server+"/post-deal",postData_Deals.toString());
                    //loadDeals();
                    myDialog.dismiss();


                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });
*/