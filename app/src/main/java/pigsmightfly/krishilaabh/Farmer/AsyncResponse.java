package pigsmightfly.krishilaabh.Farmer;

/**
 * Created by saite on 3/12/2018.
 */

public interface AsyncResponse {
    void processFinish(String output);
}