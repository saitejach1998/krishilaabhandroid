package pigsmightfly.krishilaabh.Wholesaler;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steelkiwi.library.view.BadgeHolderLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.Farmer.DealClass;
import pigsmightfly.krishilaabh.R;

import static android.content.Context.MODE_PRIVATE;

public class WholesalerDealsFrag extends Fragment {
    public int cartcount;
    RecyclerView recycler;
    private ProgressDialog dialog;
    BadgeHolderLayout badgeHolderLayout;
    ImageButton cartButton;
    public static WholesalerDealsFrag newInstance() {
        WholesalerDealsFrag fragment = new WholesalerDealsFrag();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {

        super.onResume();

        badgeHolderLayout.setCount(CartCount.getCount());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_wholesaler_deals, container, false);
        recycler = view.findViewById(R.id.recyclerViewWholesaler);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        loadDeals();
        cartButton  = view.findViewById(R.id.imageButton);
        badgeHolderLayout = (BadgeHolderLayout)view.findViewById(R.id.badge);
        dialog = ProgressDialog.show(getActivity(), "",
                "Loading. Please wait...", true);
        SharedPreferences  prefs = getContext().getSharedPreferences("Wholesaler", MODE_PRIVATE);
       badgeHolderLayout.setCount(prefs.getInt("cartnum",0));
        cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getActivity(),CartPage.class);
                startActivity(i);

            }
        });
        return view;
    }

    public void loadDeals() {
        ((WholesalerLanding)getActivity()).getLocation();
        RequestQueue myqueue = Volley.newRequestQueue(getActivity());
        final android.support.v7.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), android.R.style.Theme_DeviceDefault_Light_Dialog_Alert);
        } else {
            builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        }
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST,  "http://192.168.43.65:5001/get-all-deal", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                if (response == null)
                    System.out.println("Response is nuxll");
                else {
                    System.out.println("from wholesaler"+response);
                    loadDeals(response);

                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.


                builder.setTitle("Connection error")
                        .setMessage(" Failed to reach the server\n Please try again later ")
                        .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                getActivity().finish();
                            }

                        })
                        .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                loadDeals();
                            }

                        })

                        .show();
            }

        }) {
            protected Map<String, String> getParams() {
              //  SharedPreferences prefs = getActivity().getSharedPreferences("P", MODE_PRIVATE);
                Map<String, String> MyData = new HashMap<String, String>();
                SharedPreferences prefs = getActivity().getSharedPreferences("gps", MODE_PRIVATE);
                MyData.put("Latitude", prefs.getString("lat","lat not found"));
                MyData.put("Longitude", prefs.getString("lon","lon not found"));//Add the data you'd like to send to the server.

                return MyData;
            }
        };
        myqueue.add(MyStringRequest);


    }

    private void loadDeals(String response) {
        ArrayList<DealClass> dealList = new ArrayList<DealClass>() {
        };
        try {
            //converting the string to json array object
            JSONArray array = new JSONArray(response);

            //traversing through all the object
            for (int i = 0; i < array.length(); i++) {

                //getting product object from json array
                JSONObject product = array.getJSONObject(i);


                //adding the product to product list
                dealList.add(new DealClass(//String product, double quantity, String variety, String units, String id, String sellername, String address, String price
                        product.getString("Product"), product.getDouble("Quantity"), product.getString("Variety"), product.getString("Units"),product.getString("_id")));
            }

            //creating adapter object and setting it to recyclerview
            System.out.println(dealList.get(0));
            WDealsAdapter adapter = new WDealsAdapter(getActivity(), dealList,badgeHolderLayout,WholesalerDealsFrag.this);
            recycler.setAdapter(adapter);
            // RecyclerView r = findViewById(R.id.recyclerView);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
