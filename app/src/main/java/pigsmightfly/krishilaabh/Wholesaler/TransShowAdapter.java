package pigsmightfly.krishilaabh.Wholesaler;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import pigsmightfly.krishilaabh.Farmer.DealClass;
import pigsmightfly.krishilaabh.R;
import pigsmightfly.krishilaabh.Transporter.TransporterClass;

/**
 * Created by saite on 3/25/2018.
 */


public class TransShowAdapter extends RecyclerView.Adapter<TransShowAdapter.ProductViewHolder> {

    private Dialog dialog_transdetails;
    private Context mCtx;
    private ArrayList<TransporterClass> transList;
    private String uniq;

    public TransShowAdapter(Context mCtx, ArrayList<TransporterClass> transList) {
        this.mCtx = mCtx;
        this.transList = transList;

    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.transporter_list, null);
        return new ProductViewHolder(view);
    }




    public void onBindViewHolder(ProductViewHolder holder, int position) {
       final TransporterClass trans = transList.get(position);
        holder.AgencyName.setText(trans.getAgency());
        //caller button
        holder.caller_in_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri number = Uri.parse("tel:"+trans.getPhone_number());
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                mCtx.startActivity(callIntent);
            }
        });

        //onclick transporter cart
        holder.onclick_trans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowTransPopup(view,trans);
            }
        });




    }


    @Override
    public int getItemCount() {
        return transList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView AgencyName, UniqueCode;
        ImageView caller_in_list;
        CardView onclick_trans;
        public ProductViewHolder(View itemView) {
            super(itemView);
            AgencyName = itemView.findViewById(R.id.trans_agency_name);
            //UniqueCode = itemView.findViewById(R.id.quantity);
            caller_in_list = itemView.findViewById(R.id.caller_in_list);
            onclick_trans = itemView.findViewById(R.id.trans_card_click);
        }
    }

    private void ShowTransPopup(View v,TransporterClass trans){
        final TransporterClass tran = trans;
        dialog_transdetails  = new Dialog(mCtx);
        dialog_transdetails.setContentView(R.layout.transporter_details);
        final TextView agency_name  = dialog_transdetails.findViewById(R.id.OwnerName_text);
        //final TextView  driver_name = dialog_transdetails.findViewById(R.id.driverNameText);
        final TextView address_agency  = dialog_transdetails.findViewById(R.id.trans_agency_adr_popup);
        final  TextView agency_phone = dialog_transdetails.findViewById(R.id.trans_phone_popup);
        final ImageView call = dialog_transdetails.findViewById(R.id.callicon_popup);
        final TextView type = dialog_transdetails.findViewById(R.id.transporterDetailsType);
        final TextView popup_close_trans = dialog_transdetails.findViewById(R.id.close_trans_dialog);

        agency_name.setText(trans.getAgency());
      //  driver_name.setText(trans.g);
        address_agency.setText(tran.getAddress());
        agency_phone.setText(tran.getPhone_number());
        type.setText(tran.getType());
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri number = Uri.parse("tel:"+tran.getPhone_number());
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                mCtx.startActivity(callIntent);
            }
        });

        popup_close_trans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_transdetails.dismiss();
            }
        });

        dialog_transdetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_transdetails.show();



    }

}
