package pigsmightfly.krishilaabh.Wholesaler;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.karan.churi.PermissionManager.PermissionManager;

import java.util.WeakHashMap;

import pigsmightfly.krishilaabh.Manifest;
import pigsmightfly.krishilaabh.R;

public class WholesalerLanding extends AppCompatActivity{
    PermissionManager permission;
    private TextView mTextMessage;
    boolean doubleBackToExitPressedOnce = false;
    private LocationManager locationManager;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.wholesaler_deals:
                    loadFragment(new WholesalerDealsFrag());


                    return true;
                case R.id.krishi_daily:

                    //loadFragment(new KrishDaily());
                    return true;
                case R.id.krishi_Bazaar:
                    Uri gmmIntentUri = Uri.parse("geo:0,0?q=restaurants");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wholesaler_landing);
        permission =new PermissionManager() {};
        Intent gsignin = getIntent();
        permission.checkAndRequestPermissions(this);
        /*
        loadFragment(new WholesalerDealsFrag());
        if(ActivityCompat.checkSelfPermission(WholesalerLanding.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(WholesalerLanding.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        */

        if(gsignin.getExtras()!=null){
            SharedPreferences.Editor editor = getSharedPreferences("Wholesaler", MODE_PRIVATE).edit();
            editor.putString("email",gsignin.getStringExtra("email"));
            editor.commit();

        }



        SharedPreferences w = getSharedPreferences("Wholesaler",MODE_PRIVATE);
        CartCount.setCount(w.getInt("cartnum",0));
        SharedPreferences p = getSharedPreferences("P",MODE_PRIVATE);
        //
        if(p.getBoolean("isFirstTime",true)==false){
            loadFragment(new WholesalerDealsFrag());
            getLocation();
        }
        SharedPreferences.Editor editor1 = getSharedPreferences("P", MODE_PRIVATE).edit();
        editor1.putBoolean("isFirstRun",false);
        editor1.putString("User","Wholesaler");
        editor1.commit();

        if(ActivityCompat.checkSelfPermission(WholesalerLanding.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&  ActivityCompat.checkSelfPermission(WholesalerLanding.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            getLocation();

        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        permission.checkResult(requestCode,permissions, grantResults);
        loadFragment(new WholesalerDealsFrag());
    }



    public boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    public void getLocation() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(WholesalerLanding.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (WholesalerLanding.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(WholesalerLanding.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            Toast.makeText(this, "Enable location services for accurate data", Toast.LENGTH_SHORT).show();
        }

        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                SharedPreferences.Editor editor = getSharedPreferences("gps", MODE_PRIVATE).edit();
                System.out.println("GPS"+location.getLatitude()+location.getLongitude());
                editor.clear();
                editor.putString("lat",String.valueOf(latti));
                editor.putString("lon",String.valueOf(longi));
                editor.commit();


            }

            if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                System.out.println("GPS"+location.getLatitude()+location.getLongitude());
                SharedPreferences.Editor editor = getSharedPreferences("gps", MODE_PRIVATE).edit();
                editor.clear();
                editor.putString("lat",String.valueOf(latti));
                editor.putString("lon",String.valueOf(longi));
                editor.commit();


            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                System.out.println("GPS"+location.getLatitude()+location.getLongitude());
                SharedPreferences.Editor editor = getSharedPreferences("gps", MODE_PRIVATE).edit();
                editor.clear();
                editor.putString("lat",String.valueOf(latti));
                editor.putString("lon",String.valueOf(longi));
                editor.commit();

            }else{

                Toast.makeText(this,"Unable to Trace your location", Toast.LENGTH_SHORT).show();

            }

    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);




    }




}
