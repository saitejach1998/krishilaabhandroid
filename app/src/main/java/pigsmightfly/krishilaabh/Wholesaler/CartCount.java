package pigsmightfly.krishilaabh.Wholesaler;

/**
 * Created by saite on 3/24/2018.
 */

public final class CartCount {
    public static int count;

    public static void setCount(int count) {
        CartCount.count = count;
    }

    public static int getCount() {

        return count;
    }

    public static void increment(){
        count++;
    }

    public static void decrement(){
        if(count > 0)
        count--;
    }
}
