package pigsmightfly.krishilaabh.Wholesaler;

/**
 * Created by saite on 3/13/2018.
 */
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steelkiwi.library.view.BadgeHolderLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.Farmer.DealClass;
import pigsmightfly.krishilaabh.Farmer.FarmerDeals;
import pigsmightfly.krishilaabh.Manifest;
import pigsmightfly.krishilaabh.R;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static pigsmightfly.krishilaabh.Farmer.FarmerLogin.server;

/**
 * Created by saite on 3/13/2018.
 */

public class WDealsAdapter extends RecyclerView.Adapter<WDealsAdapter.ProductViewHolder> {

    private int count = 0;
    private Context mCtx;
    private  WholesalerDealsFrag frag;
    private ArrayList<DealClass> dealList;
    Dialog ItemDialog;
    private String  response1 = null;
    LocationListener locationManager;
    ProgressDialog dialog;

    BadgeHolderLayout badge;

    public WDealsAdapter(Context mCtx, ArrayList<DealClass> dealList, BadgeHolderLayout badgeHolderLayout,WholesalerDealsFrag frag) {
        this.mCtx = mCtx;
        this.dealList = dealList;
        badge = badgeHolderLayout;
        this.frag = frag;
}

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.deal_list2, null);

        return new ProductViewHolder(view);
    }


    public void onBindViewHolder(ProductViewHolder holder, final int position) {
        DealClass deal = dealList.get(position);
        ItemDialog = new Dialog(mCtx, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        holder.cropName.setText(deal.getProduct());
        holder.quantity.setText(deal.getQuantity() + " " + deal.getUnits());
        holder.variety.setText(deal.getVariety());
        System.out.println(deal.getId());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on" + position);

                 GetApproxTime(dealList.get(position), view,position);
                dialog = ProgressDialog.show(mCtx, "",
                        "Loading. Please wait...", true);

                /*
                Intent i = new Intent(mCtx,WDealPage.class);
                i.putExtra("Name",dealList.get(position).getProduct());
                i.putExtra("Quantity",dealList.get(position).getQuantity());
                i.putExtra("Variety",dealList.get(position).getVariety());
                i.putExtra("Units",dealList.get(position).getUnits());
                mCtx.startActivity(i);
                */
            }
        });

    }

    @Override
    public int getItemCount() {
        return dealList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView cropName, quantity, price, variety;
        RelativeLayout relativeLayout;

        public ProductViewHolder(View itemView) {
            super(itemView);
            cropName = itemView.findViewById(R.id.CropName);
            quantity = itemView.findViewById(R.id.quantity);
            price = itemView.findViewById(R.id.price);
            variety = itemView.findViewById(R.id.variety);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout);

        }
    }

    public void ShowPopup(View v, DealClass deal,String res) {
        final DealClass addDeal = deal;
        TextView txtclose;
        Button btndeals;
        JSONObject responseObject = null;

        ItemDialog.setContentView(R.layout.activity_wholesaleron_click_deal);
        //TextView s = ItemDialog.findViewById(R.id.distance)
        TextView sellername = ItemDialog.findViewById(R.id.sellername);
        TextView selleraddress = ItemDialog.findViewById(R.id.selleraddress);
        TextView sellercrop = ItemDialog.findViewById(R.id.sellercrop);
        TextView sellervariety = ItemDialog.findViewById(R.id.sellervariety);
        TextView sellerunit = ItemDialog.findViewById(R.id.sellerunit);
        TextView sellerprice = ItemDialog.findViewById(R.id.sellerprice);
        TextView estimateTime = ItemDialog.findViewById(R.id.estimatetime);
        TextView distance = ItemDialog.findViewById(R.id.textView8);
       //TextView distance = ItemDialog.findViewById(R.id.dista);
        //TextView sellerquant = ItemDialog.findViewById(R.id.seller)
        sellercrop.setText(deal.getProduct());
        System.out.println("Time"+res);
        //estimateTime.setText(response1);

        try {

            responseObject = new JSONObject(response1);
            estimateTime.setText(responseObject.getString("time"));
            sellername.setText(responseObject.getString("name"));
            selleraddress.setText(responseObject.getString("address"));
            sellerprice.setText(responseObject.getString("price"));
            distance.setText(responseObject.getString("distance"));
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        System.out.println(response1);
        sellerunit.setText(deal.getUnits());
        sellervariety.setText(deal.getVariety());
        //sellerprice.setText(deal.getPrice());
        //sellername.setText(deal.getSellername());
        //selleraddress.setText(deal.getAddress());
        CardView AddTOCart =  ItemDialog.findViewById(R.id.addtocartbutton);
        AddTOCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddItemCart(addDeal);

            }
        });
        dialog.dismiss();
        //displaying dialog
        ItemDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ItemDialog.show();


    }

    private void GetApproxTime(final DealClass deal,final View view,final int position) {


        final RequestQueue myqueue = Volley.newRequestQueue(mCtx);
        final android.support.v7.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(mCtx, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new android.support.v7.app.AlertDialog.Builder(mCtx);
        }

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, "http://192.168.43.65:5010" + "/get-distance", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String toBeSent;
               if(response.compareTo("no-deals")==0){
                response1 = "Unknown";
                toBeSent = "Unknown";
               }

               else if(response.compareTo("not-found")==0){
                   response1 = "Unknown";
                   toBeSent = "Unknown";
               }
               else {
                   response1 = response;
                   toBeSent = response;
               }

                ShowPopup(view, dealList.get(position),toBeSent);
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                System.out.println(error);

                builder.setTitle("Connection  Failure")
                        .setMessage(" Could not reach the server\n Please try again later")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }

                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() {
                SharedPreferences prefs = mCtx.getSharedPreferences("gps", MODE_PRIVATE);
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("_id", deal.getId());
                MyData.put("Latitude", prefs.getString("lat", "lat does not exist")); //Add the data you'd like to send to the server.
                //MyData.put("Lat",)
                MyData.put("Longitude", prefs.getString("lon", "lat does not exist"));
                System.out.println(MyData);
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);

    }


    private void AddItemCart(DealClass deal){

        final DealClass deal_new = deal;
        final RequestQueue Rqueue = Volley.newRequestQueue(mCtx);
        final android.support.v7.app.AlertDialog.Builder builder2;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder2 = new android.support.v7.app.AlertDialog.Builder(mCtx, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder2 = new android.support.v7.app.AlertDialog.Builder(mCtx);
        }

        StringRequest MyStringRequest2 = new StringRequest(Request.Method.POST, "http://192.168.43.65:5001/add-cart", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                if(response.compareTo("done")==0){
                    builder2.setTitle("Deal Added ")
                            .setMessage("You have Successfully added a deal to your cart")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ItemDialog.dismiss();
                                    dialog.dismiss();
                               // badge.increment();
                                    CartCount.increment();
                                    SharedPreferences.Editor editor = mCtx.getSharedPreferences("Wholesaler", MODE_PRIVATE).edit();
                                    editor.putInt("cartnum",CartCount.getCount());
                                    editor.commit();
                                   // (WholesalerLanding)mCtx.loadFragment(new WholesalerDealsFrag());
                                //SharedPreferences.Editor editor = mCtx.getSharedPreferences("Wholesaler", MODE_PRIVATE).edit();
                                    //editor.putInt("cartnum",badge.getCount());
                                //editor.commit();
                                        frag.badgeHolderLayout.setCount(CartCount.getCount());
                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }
                else if(response.compareTo("duplicate")==0){
                    builder2.setTitle("Deal Not Added")
                            .setMessage("There is a duplicate of the same deal in your cart")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                    ItemDialog.dismiss();
                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }
                else {
                    System.out.println(response);
                }


            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                System.out.println(error);
                //dialog.dismiss();
                builder2.setTitle("Connection  Failure")
                        .setMessage(" Could not reach the server\n Please try again later")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }

                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() {
                SharedPreferences prefs = mCtx.getSharedPreferences("Wholesaler", MODE_PRIVATE);
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("id",deal_new.getId());
                MyData.put("email", prefs.getString("email", "email does not exist")); //Add the data you'd like to send to the server.
                //MyData.put("Lat"n,)
                MyData.put("op", "1");
                System.out.println(MyData);
                return  MyData;
            }
        };
        Rqueue.add(MyStringRequest2);



    }



    }

