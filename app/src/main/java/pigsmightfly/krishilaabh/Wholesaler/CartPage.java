package pigsmightfly.krishilaabh.Wholesaler;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.Farmer.DealClass;
import pigsmightfly.krishilaabh.R;

public class CartPage extends AppCompatActivity {
    RecyclerView recycler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_page);
        recycler = findViewById(R.id.cartRecycler);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        loadDeals();

    }

    public void loadDeals() {

        RequestQueue myqueue = Volley.newRequestQueue(this);
        final android.support.v7.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new android.support.v7.app.AlertDialog.Builder(this);
        }
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST,  "http://192.168.43.65:5001/view-cart", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                if (response.compareTo("no-deal")==0) {
                    System.out.println("Response is null");
                    loadDeals("0");
                }
                else {
                    System.out.println("from cart"+response);
                    loadDeals(response);

                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.


                builder.setTitle("Connection error")
                        .setMessage(" Failed to reach the server\n Please try again later ")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                loadDeals();
                            }

                        })
                        .setIcon(android.R.drawable.alert_light_frame)
                        .show();
            }

        }) {
            protected Map<String, String> getParams() {
                SharedPreferences prefs = getSharedPreferences("Wholesaler", MODE_PRIVATE);
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("email", prefs.getString("email","email does not exist")); //Add the data you'd like to send to the server.
                System.out.println(MyData);
                return MyData;
            }
        };
        myqueue.add(MyStringRequest);


    }

    public void loadDeals(String response) {

        if(response.compareTo("0")!=0) {
            //setContentView(R.layout.activity_cart_page);
            ArrayList<DealClass> dealList = new ArrayList<DealClass>() {
            };
            try {
                //converting the string to json array object
                JSONArray array = new JSONArray(response);

                //traversing through all the object
                for (int i = 0; i < array.length(); i++) {

                    //getting product object from json array
                    JSONObject product = array.getJSONObject(i);

                    //adding the product to product list
                    dealList.add(new DealClass(
                            product.getString("Product"), product.getDouble("Quantity"), product.getString("Variety"), product.getString("Units"), product.getString("_id")

                    ));
                }

                //creating adapter object and setting it to recyclerview
                System.out.println(dealList.get(0));
                CartPageAdapter adapter = new CartPageAdapter(this, dealList);
                recycler.setAdapter(adapter);

                // RecyclerView r = findViewById(R.id.recyclerView);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            setContentView(R.layout.emptycart);

        }
    }

}
