package pigsmightfly.krishilaabh.Wholesaler;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationListener;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steelkiwi.library.view.BadgeHolderLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pigsmightfly.krishilaabh.Farmer.DealClass;
import pigsmightfly.krishilaabh.R;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by saite on 3/23/2018.
 */

public class CartPageAdapter extends RecyclerView.Adapter<CartPageAdapter.ProductViewHolder> {

    private int count = 0;
    private Context mCtx;
    private ArrayList<DealClass> dealList;
    Dialog ItemDialog;
    private String response1 = null;
   // BadgeHolderLayout badge;


    public CartPageAdapter(Context mCtx, ArrayList<DealClass> dealList) {
        this.mCtx = mCtx;
        this.dealList = dealList;
       // badge = badgeHolderLayout;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.deal_list, null);


        return new ProductViewHolder(view);
    }


    public void onBindViewHolder(ProductViewHolder holder, final int position) {

        final DealClass deal = dealList.get(position);
        holder.cropName.setText(deal.getProduct());
        holder.quantity.setText(deal.getQuantity() + " " + deal.getUnits());
        holder.variety.setText(deal.getVariety());
        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RemoveItemCart(deal);

            }
        });
        holder.transporter_direct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mCtx,TransShowPage.class);
                i.putExtra("id",deal.getId());
                mCtx.startActivity(i);
            }
        });
        System.out.println(deal.getId());


    }

    @Override
    public int getItemCount() {
        return dealList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView cropName, quantity, price, variety;
        RelativeLayout relativeLayout;
        TextView removeButton;
        ImageView transporter_direct;

        public ProductViewHolder(View itemView) {
            super(itemView);
            removeButton = (TextView) itemView.findViewById(R.id.removeButton);
            cropName = itemView.findViewById(R.id.CropName);
            quantity = itemView.findViewById(R.id.quantity);
            price = itemView.findViewById(R.id.price);
            variety = itemView.findViewById(R.id.variety);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout);
            transporter_direct = itemView.findViewById(R.id.transporter_direct);
        }
    }

    private void RemoveItemCart(DealClass deal){

        final DealClass deal_new = deal;
        final RequestQueue Rqueue = Volley.newRequestQueue(mCtx);
        final android.support.v7.app.AlertDialog.Builder builder2;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder2 = new android.support.v7.app.AlertDialog.Builder(mCtx, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder2 = new android.support.v7.app.AlertDialog.Builder(mCtx);
        }

        StringRequest MyStringRequest2 = new StringRequest(Request.Method.POST, "http://192.168.43.65:5001/add-cart", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                if(response.compareTo("removed")==0){
                    builder2.setTitle("Deal Removed ")
                            .setMessage("You have Successfully removed a deal from  your cart")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    ((CartPage)mCtx).loadDeals();
                                    CartCount.decrement();
                                   /// badge.decrement();

                                    SharedPreferences.Editor editor = mCtx.getSharedPreferences("Wholesaler", MODE_PRIVATE).edit();
                                    editor.putInt("cartnum",CartCount.getCount());
                                    editor.commit();

                                }

                            })
                            .setIcon(android.R.drawable.alert_light_frame)
                            .show();
                }


                else {
                    System.out.println(response);
                }


            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                System.out.println(error);
                //dialog.dismiss();
                builder2.setTitle("Connection  Failure")
                        .setMessage(" Could not reach the server\n Please try again later")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }


                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() {
                SharedPreferences prefs = mCtx.getSharedPreferences("Wholesaler", MODE_PRIVATE);
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("id",deal_new.getId());
                MyData.put("email", prefs.getString("email", "email does not exist")); //Add the data you'd like to send to the server.
                //MyData.put("Lat"n,)
                MyData.put("op", "0");
                System.out.println(MyData);
                return  MyData;
            }
        };
        Rqueue.add(MyStringRequest2);



    }

}
