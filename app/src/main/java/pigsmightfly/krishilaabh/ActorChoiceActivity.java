package pigsmightfly.krishilaabh;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import pigsmightfly.krishilaabh.Farmer.FarmerLanding;
import pigsmightfly.krishilaabh.Farmer.FarmerLogin;
import pigsmightfly.krishilaabh.Transporter.TransporterLanding;
import pigsmightfly.krishilaabh.Transporter.TransporterRegister;
import pigsmightfly.krishilaabh.Wholesaler.WholesalerDealsFrag;
import pigsmightfly.krishilaabh.Wholesaler.WholesalerLanding;
import pigsmightfly.krishilaabh.Wholesaler.WholesalerLogin;

public class ActorChoiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actorchoice);


        TextView textView = (TextView) findViewById(R.id.MainPageTitle);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/text.ttf");
        textView.setTypeface(typeface);

        TextView textView1 = (TextView) findViewById(R.id.ConsumerTextBox);
        Typeface typeface1 = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");
        textView1.setTypeface(typeface1);

        TextView textView2 = (TextView) findViewById(R.id.TransporterTextBox);
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");
        textView2.setTypeface(typeface2);

        TextView textView3 = (TextView) findViewById(R.id.WholesalerTextBox);
        Typeface typeface3 = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");
        textView3.setTypeface(typeface3);

        TextView textView4 = (TextView) findViewById(R.id.FarmerTextBox);
        Typeface typeface4 = Typeface.createFromAsset(getAssets(), "fonts/Rajdhani-Medium.ttf");
        textView4.setTypeface(typeface4);


        Boolean isFirstTime = getSharedPreferences("P", MODE_PRIVATE).getBoolean("isFirstRun", true);
        System.out.println(isFirstTime);
        ////////change here/////////////////////// isFirstTime -> false
        if (isFirstTime) {

            //first time



            CardView f_card = (CardView) findViewById(R.id.farmer_card);
            CardView c_card = (CardView) findViewById(R.id.consumer_card);
            CardView w_card = (CardView) findViewById(R.id.wholaslaer_card);
            CardView t_card = (CardView) findViewById(R.id.transporter_card);

            f_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   // Toast.makeText(ActorChoiceActivity.this, "Farmer Login", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(ActorChoiceActivity.this, FarmerLogin.class);
                    startActivity(i);

                    /*
                    editor.putString("userType","Farmer");
                    editor.putBoolean("isFirstRun",)
                    */
                    /*open respective login pages..... After Sucessful login ...
                    have this
                    editor.putString("userType","Farmer");
                    editor.putBoolean("isFirstRun",)
                    */
                }
            });

            c_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // Toast.makeText(ActorChoiceActivity.this, "Customer Login", Toast.LENGTH_LONG).show();
                }
            });

            w_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // Toast.makeText(ActorChoiceActivity.this, "Wholesaler Login", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(ActorChoiceActivity.this, WholesalerLogin.class);
                    startActivity(i);
                }
            });
            t_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(ActorChoiceActivity.this, "Transporter Login", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(ActorChoiceActivity.this, TransporterLanding.class);
                    startActivity(i);
                }
            });
        }
        else {
//              Farmer
            String user = getSharedPreferences("P",MODE_PRIVATE).getString("User","none");
            Intent i;
            switch (user){

                case "Farmer":
                    //open farmer main page
                    //Toast.makeText(ActorChoiceActivity.this, "Farmer", Toast.LENGTH_LONG).show();
                     i = new Intent(ActorChoiceActivity.this, FarmerLanding.class);
                    startActivity(i);
                    break;

                case "Customer":
                    //open customer home page
                    //Toast.makeText(ActorChoiceActivity.this, "Customer", Toast.LENGTH_LONG).show();break;
                case "Transporter":
                    i = new Intent(ActorChoiceActivity.this, TransporterLanding.class);
                    startActivity(i);
                    break;
                case "Wholesaler":
                    //open wholesaler home page
                    //Toast.makeText(ActorChoiceActivity.this, "Transporter", Toast.LENGTH_LONG).show();
                    i = new Intent(ActorChoiceActivity.this, WholesalerLanding.class);
                    startActivity(i);
                    break;

                    default:  Toast.makeText(ActorChoiceActivity.this, user, Toast.LENGTH_LONG).show();;break;

            }

        }


    }









}
